const express = require("express")
const routes = require("./routes")
const next = require("next")
const path = require("path")

const PORT = parseInt(process.env.PORT, 10) || 3000
const app = next({ dev: process.env.NODE_ENV !== "production" })
const handle = routes.getRequestHandler(app)

function createServer() {
    const server = express()
    server.use("/_next", express.static(path.join(__dirname, ".next")))
    server.use(handle)
    return server
}

const server = createServer()

const prepareProcess = app.prepare().then(() => {
    console.log("App prepared")
    if (process.env.IN_LAMBDA !== "true") {
        console.log(`Started server on: ${PORT}`)
        server.listen(PORT, err => {
            if (err) throw err
            console.log(`> Ready on http://localhost:${PORT}`)
        })
    }
})

module.exports = {appServer: server, prepareProcess}
