const routes = module.exports = require("next-routes")()
 
routes
    .add("blog", "/blog", "/blog")
    .add("calculator", "/:service?/:services?/price-estimator/:company?", "/calculator")
    .add("blog-post", "/blog/:slug", "/blog/post")
    .add("company", "/company/:slug", "/company")
    .add("services", "/service/:slug", "/service")
    .add("pricing", "/pricing/:slug", "/pricing")
    .add("home", "/:home?/:service?/:services?/:company?", "/home")
    .add("contact", "/contact/:service?/:services?/:company?", "/contact")
    .add("disclaimer", "/disclaimer/:service?/:services?/:company?", "/disclaimer")
    .add("privacy", "/privacy/:service?/:services?/:company?", "/privacy")

    