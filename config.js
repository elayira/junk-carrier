const Amplify = require('@aws-amplify/core').default
const PRISMIC_API_URL = "https://junkyard.cdn.prismic.io/api/v2"

const cms_secrets = require("./services/api/cms")
const crm_secrets = require("./services/api/crm")


const DEFAULT_SEO = {
    title: "Junk-Yard",
    shortDescription: "America's Best Junk Service",
    description: "Best Junk Removal Service",
    openGraph: {
        type: "website",
        locale: "en_IE",
        url: cms_secrets["DOMAIN"],
        title: "Junkyard",
        description: "Junkyard",
        image:
            "/static/images/logos/android-chrome-512x512.png",
        site_name: cms_secrets["SITE_NAME"],
        imageWidth: 512,
        imageHeight: 512
    },
    twitter: {
        handle: "@junkyard",
        cardType: "summary_large_image"
    }
}

const SOCIAL_MEDIA = [
    {
        name: "facebook",
        url: "https://www.facebook.com/AbokiFx-Rate-305191360158341/?modal=admin_todo_tour"
    },
    {
        name: "twitter",
        url: "https://twitter.com/AbokifxR"
    }
]

function configureAmplify() {
    return Amplify.configure({
        Auth: {
            mandatorySignIn: true,
            region: "us-east-1",
            userPoolId: "us-east-1_j7bUsLdId",
            identityPoolId: "us-east-1:544b6c44-c1f4-468b-8f82-5263a508f559",
            userPoolWebClientId: "44mhgico5hfg2mbpducp4khcop"
        },
        Storage: {
            region: "us-east-1",
            bucket: "com.junkcarrier.api-us-east-1production",
            identityPoolId: "us-east-1:544b6c44-c1f4-468b-8f82-5263a508f559"
        },
        API: {
            endpoints: [
                {
                    name: "pages",
                    endpoint: "http://localhost:5000",
                    region: "us-east-1"
                },
                {
                    name: "doc",
                    endpoint: "http://localhost:5000",
                    region: "us-east-1"
                },
                {
                    name: "deal",
                    endpoint: "http://localhost:5000",
                    region: "us-east-1"
                }
            ]
        }
    })
}



module.exports = { DEFAULT_SEO, PRISMIC_API_URL, SOCIAL_MEDIA, configureAmplify }

