import { success, failure } from "../helper"
import Axios from "axios"
import { hubspotEndpoint } from "../helper"


export function scheduleDeal(event, context, callback) {
    const apiKey = "a1588c40-5094-4ab4-88e0-dd1c3a052c3a"
    const {contact, deal} = JSON.parse(event.body)
    try {
            Axios.post(
                hubspotEndpoint(apiKey),
                contact
            )
            .then((contact) => callback(null, success(contact)))
            .catch((err) => callback(null, failure(err)))
    } catch (error) {
        return callback(null, failure(error))
    }

}
