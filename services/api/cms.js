import Prismic from "prismic-javascript"

import { PRISMIC_API_URL } from"../../config"
import {success, failure, isEmpty} from "../helper"

export async function pages(event) {
    const {...data} = event.queryStringParameters
    try {
        const API = await Prismic.api(PRISMIC_API_URL);
        const {results} = await API.query(
            Prismic.Predicates.at("document.type", data.pageID),
            {
                orderings: "[document.last_publication_date desc]",
                ...data.params
            }
        )
        return isEmpty(results)? failure({}):success(results.length === 1? results[0]: results)
    } catch (error) {
        return failure(err)
    }
}

export async function doc(event, context, callback) {
    const {...data} = event.queryStringParameters
    try {
        const API = await Prismic.api(PRISMIC_API_URL);
        const {results} = await API.query(
            Prismic.Predicates.at(`my.${data.pageID}.uid`, data.slug)
        );
        return success(results[0])
    } catch (error) {
        return failure(err)
    }
}
