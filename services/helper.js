export function success(body) {
    return buildResponse(200, body);
}

export function failure(body) {
    return buildResponse(404, body);
}

export function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export function hubspotEndpoint(apikey) {
    return `https://api.hubapi.com/contacts/v1/contact/?hapikey=${apikey}`
}

function buildResponse(statusCode, body) {
    return {
        statusCode: statusCode,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(body)
    };
}

