 const sls = require("serverless-http")
 const {binaryMimeTypes} =  require("./binaryMimeTypes")
 const server  = require("../../server")

process.env.IN_LAMBDA = true
process.env.NODE_ENV = "production"

let handler = null

module.exports.app = (event, context, callback) => {
    const {appServer, prepareProcess} = server
    let initializerProcess

    if(handler === null) {
        initializerProcess = prepareProcess.then( () => {
            handler = sls(appServer, {binary: binaryMimeTypes})
        })
    } else {
        initializerProcess = Promise.resolve()
    }

    initializerProcess
    .then(() => handler(event, context, callback))
    .catch(err => console.log("failure", err))
}
