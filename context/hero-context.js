import React from "react"

import {isMobile} from "../components/helpers/utility"

export default React.createContext(
    {
        primary: {
            image: "",
            header:"",
            subheader: ""
        },
        fields: [],
        isMobile
    }
)
