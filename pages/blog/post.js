import React from "react"
import Layout from "../../layouts"
import {
    Header,
} from "semantic-ui-react"


class Post extends React.Component {
    render() {
        return (
            <Layout>
                <Header content="Post Page" as="h1" textAlign="center" inverted/>
            </Layout>
        )
    }
}

export default Post
