import React from "react"
import Layout from "../../layouts"
import {
    Header,
} from "semantic-ui-react"


class Blog extends React.Component {
    render() {
        return (
            <Layout>
                <Header content="Blog Page" as="h1" textAlign="center" inverted/>
            </Layout>
        )
    }
}

export default Blog
