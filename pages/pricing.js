import React from "react"

import {
    Header,
    Grid,
    Container
} from "semantic-ui-react"

import Layout from "../layouts"
import Hero from "../components/Hero"
import MainBanner from "../components/MainBanner"

const hero = {
    body: {
        image: "/static/images/cargo.jpg",
        content: "Company's main banner message goes here",
        subheader: "Company sub banner message goes here"
    },
    banner: [
        {
            image: "/static/images/delivery-truck.png",
            content: "BOOK ONLINE",
            subheader: "and Save $20"
        },
        {
            image: "/static/images/phone-receiver.png",
            content: "CALL US",
            subheader: "1-888-888-JUNK (5865)"
        },
        {
            image: "/static/images/text.png",
            content: "TEXT US @",
            subheader: "1-737-888-5865"
        }
    ]
}



const Pricing = (props) => (
    <Layout Hero={<Hero body={props.body} banner={props.banner} />}>
        <Grid columns="two">
            <Grid.Column width="ten">
                <Container text>
                    <Header content="Pricing Details Goes Here" as="h1" />
                </Container>
            </Grid.Column>
            <Grid.Column width="six">
                <MainBanner />
            </Grid.Column>
        </Grid>
    </Layout>
)


Pricing.getInitialProps = () => {
    return { ...hero }
}

export default Pricing
