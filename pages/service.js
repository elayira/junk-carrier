import React, {
    useEffect,
    useState
} from "react"

import {
    Grid,
    Container
} from "semantic-ui-react"
import { RichText } from "prismic-reactjs"
const API = require('@aws-amplify/api').default

import Layout from "../components/layouts"
import MainBanner from "../components/MainBanner"
import { linkResolver } from "../components/helpers/utility"


const Service = (props) => {
    return (
        <Layout>
            <Grid columns="two" stackable reversed="mobile">
                <Grid.Column width="ten">
                    <Container text>
                        {/* {console.log(props.data)} */}
                        {RichText.render(props.data.body, linkResolver)}
                    </Container>
                </Grid.Column>
                <Grid.Column width="six">
                    <MainBanner />
                </Grid.Column>
            </Grid>
        </Layout>
    )
}


Service.getInitialProps = async ({ query }) => {
    const { slug } = query
    const {data} = await API.get("doc", "/detail", {
        headers: {},
        response: false,
        queryStringParameters: { pageID: 'services', slug: slug }
    })
    return {
        data
    }
}

export default Service
