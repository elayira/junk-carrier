import React from "react"

import {
    Grid,
    Container
} from "semantic-ui-react"
import { RichText } from "prismic-reactjs"
const API = require('@aws-amplify/api').default

import Layout from "../components/layouts"
import MainBanner from "../components/MainBanner"
import { linkResolver } from "../components/helpers/utility"


const Contact = (props) => (
    <Layout>
        <Grid columns="two" stackable reversed="mobile">
            <Grid.Column width="ten">
                <Container text>
                    {RichText.render(props.data.body, linkResolver)}
                </Container>
            </Grid.Column>
            <Grid.Column width="six">
                <MainBanner />
            </Grid.Column>
        </Grid>
    </Layout>
)


Contact.getInitialProps = async () => {
    const { data } = await API.get("pages", "/list", {
        headers: {},
        response: false,
        queryStringParameters: { pageID: 'contact', params: {} }
    })
    return {
        data
    }
}

export default Contact
