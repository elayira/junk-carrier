import React from "react"

import {
    Grid,
    Container
} from "semantic-ui-react"
import { RichText } from "prismic-reactjs"
const API = require('@aws-amplify/api').default

import Layout from "../components/layouts"
import MainBanner from "../components/MainBanner"
import { linkResolver } from "../components/helpers/utility"


const Disclaimer = (props) => (
    <Layout>
        <Grid columns="two" stackable reversed="mobile">
            <Grid.Column width="ten">
                <Container text>
                    {RichText.render(props.data.body, linkResolver)}
                </Container>
            </Grid.Column>
            <Grid.Column width="six">
                <MainBanner />
            </Grid.Column>
        </Grid>
    </Layout>
)


Disclaimer.getInitialProps = async () => {
    const { data } = await API.get("pages", "/list", {
        headers: {},
        response: false,
        queryStringParameters: { pageID: 'disclaimer', params: {} }
    })
    return {
        data
    }
}

export default Disclaimer
