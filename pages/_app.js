import App, { Container } from "next/app"
import React from "react";
import Head from "next/head"
import withNProgress from "next-nprogress"

import "../static/css/app.css"
import { DEFAULT_SEO, PRISMIC_API_URL, configureAmplify } from "../config"
import {buildHero} from "../components/helpers/utility"
import HeroContext from "../context/hero-context"
import {isEmpty} from "../services/helper"

configureAmplify()

class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {};
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }

        return { pageProps }
    }

    render() {
        const { Component, pageProps } = this.props
        return (
            <Container>
                <Head>
                    <link rel="apple-touch-icon" sizes="180x180" href="/static/favicon.ico" />
                    <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon.ico" />
                    <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon.ico" />
                    <link rel="manifest" href="/static/site.webmanifest"></link>
                    <link href="https://unpkg.com/basscss@8.0.2/css/basscss.min.css" rel="stylesheet" />
                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css" />
                    <script dangerouslySetInnerHTML={{ __html: `window.prismic = {endpoint: "${PRISMIC_API_URL}"};` }}></script>
                    <script type="text/javascript" src="//static.cdn.prismic.io/prismic.min.js"></script>
                    <title key="title">{DEFAULT_SEO.title} || {DEFAULT_SEO.description}</title>
                    <meta
                        key="description"
                        name="description"
                        content={DEFAULT_SEO.description}
                    />
                    <meta
                        key="twitter:card"
                        name="twitter:card"
                        content={DEFAULT_SEO.twitter.cardType}
                    />
                    <meta
                        key="twitter:site"
                        name="twitter:site"
                        content={DEFAULT_SEO.twitter.handle}
                    />
                    <meta
                        key="og:url"
                        property="og:url"
                        content={DEFAULT_SEO.openGraph.url}
                    />
                    <meta
                        key="og:type"
                        property="og:type"
                        content={DEFAULT_SEO.openGraph.type}
                    />
                    <meta
                        key="og:title"
                        property="og:title"
                        content={DEFAULT_SEO.openGraph.title}
                    />
                    <meta
                        key="og:description"
                        property="og:description"
                        content={DEFAULT_SEO.openGraph.description}
                    />
                    <meta
                        key="og:image"
                        property="og:image"
                        content={DEFAULT_SEO.openGraph.image}
                    />
                    <meta
                        key="og:image:width"
                        property="og:image:width"
                        content={DEFAULT_SEO.openGraph.imageWidth}
                    />
                    <meta
                        key="og:image:height"
                        property="og:image:height"
                        content={DEFAULT_SEO.openGraph.imageHeight}
                    />
                    <meta
                        key="og:locale"
                        property="og:locale"
                        content={DEFAULT_SEO.openGraph.locale}
                    />
                </Head>
                
                {pageProps.hasOwnProperty("data") && !isEmpty(pageProps.data) ? (
                    <HeroContext.Provider value={buildHero(pageProps.data['slices'])} >
                        <Component {...pageProps} />
                    </HeroContext.Provider>
                    ): <Component {...pageProps} />}
            </Container>
        );
    }
}


export default withNProgress(1000)(MyApp)

