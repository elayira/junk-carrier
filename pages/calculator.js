import React from "react"

import {
    Container
} from "semantic-ui-react"
import { RichText } from "prismic-reactjs"
const API = require('@aws-amplify/api').default

import Layout from "../components/layouts"
import { linkResolver } from "../components/helpers/utility"
import Estimator from "../components/Estimator"


const PriceEstimate = (props) => {
    const catalogue = props.data.slices.filter( slice => slice.slice_type === "item_catalogue")
    return (
        <Layout>
            <Container>
                <Estimator catalogue={catalogue} />
            </Container>
            <Container>
                {RichText.render(props.data.body, linkResolver)}
            </Container>
        </Layout>
    )
}


PriceEstimate.getInitialProps = async () => {
    const { data } = await API.get("pages", "/list", {
        headers: {},
        response: false,
        queryStringParameters: { pageID: 'price_estimator', params: {} }
    })
    return {
        data
    }
}

export default PriceEstimate
