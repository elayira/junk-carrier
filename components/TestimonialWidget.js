import React from "react"

import {
    Segment,
    Image,
    Header,
    Rating,
    Message,
} from 'semantic-ui-react'


export default () => (
    <Segment color="yellow" inverted>
        <Header as="h1" content="Why Junk King?" textAlign="center" />
        <Image src="/static/images/customer.jpg" />
        <Header 
            as="h2" 
            subheader="Read what actual customers have to say about our service!" 
            content="Our Customers Love Us!" textAlign="center" 
        />
        <Segment>
            <Message>
                <Message.Header content="Was this what you wanted?" />
                <Message.Content content="Did you know it's been a while?" />
                <Rating icon='star' defaultRating={4.5} maxRating={5} disabled />
            </Message>
            <Message>
                <Message.Header content="Was this what you wanted?" />
                <Message.Content content="Did you know it's been a while?" />
                <Rating icon='star' defaultRating={4.5} maxRating={5} disabled />
            </Message>
            <Message>
                <Message.Header content="Was this what you wanted?" />
                <Message.Content content="Did you know it's been a while?" />
                <Rating icon='star' defaultRating={4.5} maxRating={5} disabled />
            </Message>
        </Segment>
    </Segment>
)

