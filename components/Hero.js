import React, { useContext } from "react"

import LazyHero from "react-lazy-hero"
import { Segment, Header } from "semantic-ui-react"

import HeroContext from "../context/hero-context"

export default (props) => {
    const context = useContext(HeroContext)
    return context ? (
        <LazyHero
            imageSrc={context.primary.image}
            minHeight="80vh"
            style={{ postion: "relative" }}
            color="red"
            opacity={0.1}
        >
            <Segment padded style={{ backgroundColor: "#0000006b" }}>
                <Header
                    content={context.primary.header}
                    as="h1"
                    subheader={context.primary.subheader}
                    inverted
                />
            </Segment>
            <Segment.Group
                horizontal
                raised
                stacked
                style={
                    {
                        position: "absolute",
                        bottom: "-12%",
                        zIndex: "1",
                        left: "10%",
                        width: "80%",
                        textAlign: "left"
                    }
                }
            >
                {(context.isMobile() ? context.fields.slice(0, 1) : context.fields).map((field, key) => (
                    <Segment key={key}>
                        <Header
                            image={field.image}
                            as="h2"
                            color="red"
                            content={field.header}
                            subheader={field.subheader}
                            textAlign="center"
                            style={{ textTransformation: 'capitalize' }}
                        />
                    </Segment>
                ))}

            </Segment.Group>
        </LazyHero>
    ): <React.Fragment />
}

