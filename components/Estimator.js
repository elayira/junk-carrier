import React from "react"

import {
    Tab,
    Accordion,
    Icon,
    List,
    Segment,
    Checkbox,
    Container,
    Divider,
    Header,
    Grid,
    Table,
    Form,
    Popup
} from "semantic-ui-react"


class Estimator extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            catalogueItems: [],
            activeCategory: 0,
            costRange: {
                lowerBound: 0,
                higherBound: 0
            }
        }
        this.handleCategoryClick = this.handleCategoryClick.bind(this)
        this.renderCategoryTab = this.renderCategoryTab.bind(this)
        this.categoryItemHandler = this.categoryItemHandler.bind(this)
    }

    componentDidMount() {
        this.setState({ catalogueItems: this.props.catalogue })
    }

    handleCategoryClick = (_, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    categoryItemHandler(_, data) {
        const calculateCost = (cost) => {
            let currentEstimate = (data.checked? 
                                        {lowerBound: parseInt(cost.split("-")[0]), higherBound: parseInt(cost.split("-")[1])} : 
                                        {lowerBound: -parseInt(cost.split("-")[0]), higherBound: -parseInt(cost.split("-")[1])}
                                )
            return {
                lowerBound: currentEstimate.lowerBound + this.state.costRange.lowerBound,
                higherBound: currentEstimate.higherBound + this.state.costRange.higherBound
            }
        }
        data.checked? this.setState({costRange: calculateCost(data.value)}): this.setState({costRange: calculateCost(data.value)})
        console.log(calculateCost(data.value))
    }

    renderCategoryTab() {
        const { activeIndex } = this.state

        return (
            <Accordion fluid>
                {this.state.catalogueItems.map(
                    (category, key) => (<React.Fragment key={key}>
                        <Accordion.Title key={key} active={activeIndex === key} index={key} onClick={this.handleCategoryClick}>
                            <Header as="h2" style={{ textTransform: 'capitalize' }}>
                                <Icon name='dropdown' />
                                {category.primary.category_name}
                            </Header>
                            <Divider />
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === key}>
                            <Container>
                                <List horizontal>
                                    {category.items.map(
                                        (item, key) => (
                                            <React.Fragment key={key}>
                                                <List.Item>
                                                    <Segment compact style={{ textTransform: 'capitalize' }}>
                                                        <Popup
                                                            content={`$ ${item.min_cost}-${item.max_cost}`}
                                                            position="top right"
                                                            trigger={
                                                                <Checkbox onChange={this.categoryItemHandler} value={`${item.min_cost}-${item.max_cost}`} label={item.name} />
                                                            } />
                                                    </Segment>
                                                </List.Item>
                                            </React.Fragment>
                                        )
                                    )}
                                </List>
                            </Container>
                        </Accordion.Content>
                    </React.Fragment>
                    )
                )}
            </Accordion>
        )
    }

    buildCatalogue() {

        return [
            { menuItem: ({ icon: 'shop', content: 'List Items' }), render: this.renderCategoryTab },
        ]
    }

    render() {
        return (
            <React.Fragment>
                <Header textAlign="center" content="Pricing Estimator" color="grey" as="h1" />
                <Segment basic compact>
                    <Grid columns='equal' padded="vertically" stackable>
                        <Grid.Row>
                            <Grid.Column>
                                <Header content="Step 1: Enter zip code" color="red" as="h1" />
                            </Grid.Column>
                            <Grid.Column>
                                <Form size="big">
                                    <Form.Group width="equal">
                                        <Form.Input placeholder='Enter your Zip Code' name='zipcode' />
                                        <Form.Button size="big" color="red" content='GO' />
                                    </Form.Group>
                                </Form>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Grid.Column width="10">
                                    <Header content="Step 2: Click on your items for a free estimate" color="red" as="h1" />

                                </Grid.Column>
                            </Grid.Column>
                            <Grid.Column width="six">
                                <Table unstackable>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>
                                                <Header
                                                    content="Your Estimate"
                                                    color="red" as="h2"
                                                />
                                            </Table.HeaderCell>
                                            <Table.HeaderCell>
                                                <Header content={`$ ${this.state.costRange.lowerBound} - ${this.state.costRange.higherBound}`} as="h2" />
                                            </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                </Table>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Segment>
                <Tab menu={{ pointing: "true" }} panes={this.buildCatalogue()} />
            </React.Fragment>
        )
    }
}

export default Estimator
