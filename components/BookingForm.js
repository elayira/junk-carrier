import React from "react"

import { Form, Segment, Message, Loader, Dimmer } from "semantic-ui-react"
import { DateTimeInput } from 'semantic-ui-calendar-react'
import Head from "next/head"

import Geosuggest from 'react-geosuggest'
const API = require('@aws-amplify/api').default

import { configureAmplify } from "../config"


configureAmplify()


class BookingForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fullName: "",
            address: "",
            postalCode: "",
            email: "",
            dateTime: "",
            loading: false,
            state: "",
            city: "",
            phone: ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handlePlaceSelection = this.handlePlaceSelection.bind(this)
    }

    handlePlaceSelection(location) {
        if (location !== undefined) {
            this.setState({ address: location.gmaps.formatted_address })
            location.gmaps.address_components.forEach(component => {
                const { types } = component
                if (types.includes("administrative_area_level_1")) {
                    this.setState({ state: component["long_name"] })
                } else if (types.includes("administrative_area_level_2")) {
                    this.setState({ city: component["long_name"] })
                }
            })
        }

    }

    handleChange = (_, { name, value }) => this.setState({ [name]: value })

    handleSubmit(event) {
        this.setState({loading: true})
        event.preventDefault()
        const deal = {
            "associations": {

                "associatedVids": []
            },
            "properties": [
                {
                    "value": `${this.state.fullName}`,
                    "name": "Junk Removal & Hauling"
                },
                {
                    "value": "appointmentscheduled",
                    "name": "dealstage"
                },
                {
                    "value": "default",
                    "name": "pipeline"
                },
                {
                    "value": "",
                    "name": "hubspot_owner_id"
                },
                {
                    "value": `${new Date(this.state.dateTime).getTime()}`,
                    "name": "closedate"
                },
                {
                    "value": "",
                    "name": "amount"
                },
                {
                    "value": "newbusiness",
                    "name": "Junk Removal & Hauling"
                }
            ]
        }
        const contact = {
            "properties": [
                {
                    "property": "email",
                    "value": `${this.state.email}`
                },
                {
                    "property": "firstname",
                    "value": `${this.state.fullName.split(' ')[0]}`
                },
                {
                    "property": "lastname",
                    "value": `${this.state.fullName.split(' ')[1] || null}`
                },
                {
                    "property": "phone",
                    "value": `${this.state.phone}`
                },
                {
                    "property": "address",
                    "value": `${this.state.address}`
                },
                {
                    "property": "city",
                    "value": ""
                },
                {
                    "property": "state",
                    "value": ""
                },
                {
                    "property": "zip",
                    "value": `${this.state.zipCode}`
                }
            ]
        }
        API.post(
            "deal", "/schedule", {
                headers: { "Context-Type": "application/json" },
                response: false,
                body: {
                    contact,
                    deal
                }
            }
        )
        .then(contact => {
            console.log(contact)
            this.setState({loading: false})
        })
        .catch(
            err => {
                console.log(err)
                this.setState({loading: false})
        })
    }

    render() {
        return (
            <Segment inverted>
                <Head>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk0oCnDhom2QO4L-0TA4Af6_MOWhZ9PxA&libraries=places"></script>
                </Head>
                <Dimmer active={this.state.loading} page>
                    <Loader as="h1" content="Scheduling Your Remocal & Hauling Service" size="massive" />
                </Dimmer>
                <Message
                    attached
                    header="Book Online & Save $20*"
                    content="*excludes jobs $99 and under"
                />
                <Form className="attached fluid segment" inverted onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <Form.Input onChange={this.handleChange} name="fullName" placeholder="First & Last Name" width={16} inverted />
                    </Form.Group>
                    <Form.Group>
                        <Form.Field width={16}>
                            <Geosuggest placeholder="Search Address" onSuggestSelect={this.handlePlaceSelection} />
                        </Form.Field>
                    </Form.Group>
                    <Form.Group>
                        <Form.Input type="number" onChange={this.handleChange} name="postalCode" placeholder="Postal Code" width={8} />
                        <Form.Input name="phone" onChange={this.handleChange} type="number" placeholder="Phone" width={8} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Input onChange={this.handleChange} name="email" placeholder="Email" type="email" width={16} />
                    </Form.Group>
                    <Form.Group>
                        <DateTimeInput
                            name="dateTime"
                            placeholder="Pickup Date Time"
                            iconPosition="left"
                            width={16}
                            dateTimeFormat={"YYYY-MM-DDTHH:mm:ss.sssZ"}
                            value={this.state.dateTime}
                            onChange={this.handleChange}
                            closable
                        // required
                        />
                    </Form.Group>
                    <Form.Button content='BOOK IT' primary />
                </Form>
            </Segment>
        )
    }
}

export default BookingForm