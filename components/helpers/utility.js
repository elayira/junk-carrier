import {Responsive} from "semantic-ui-react"


const viewportWidth = () => {
    const isSSR = typeof window === "undefined"

    return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

function linkResolver(doc) {
    if (doc.type === "news") {
        return `/news/${doc.uid}`;
    }
    return "/";
}

export function buildHero(slices) {
    const hero = slices.filter(slice => slice.slice_type === "hero").slice(0, 1).pop()
    
    const context = hero !== undefined? {
        primary: {
            image: hero.primary.hero_body_image.url,
            header: hero.primary.hero_body_header?hero.primary.hero_body_header[0].text : "",
            subheader: hero.primary.hero_body_subheader
        },
        fields: hero.items.map(item => ({
            image: item.hero_banner_image.url,
            header: item.hero_banner_header? item.hero_banner_header[0].text : "",
            subheader: item.hero_banner_subheader
        })),
        isMobile
    } : null
    return context
}

const isMobile = () => Responsive.onlyMobile.maxWidth >= viewportWidth()

export {viewportWidth, isMobile, linkResolver}
