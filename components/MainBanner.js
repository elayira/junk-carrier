import React from "react"

import BookingForm from "./BookingForm"
import TestimonialWidget from "./TestimonialWidget"


export default () => (
    <>
        <BookingForm />
        <TestimonialWidget />
    </>
)

