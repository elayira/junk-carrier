import React, {
    useEffect,
    useState
} from "react"

import { Menu, Dropdown, Image } from "semantic-ui-react"
const API = require('@aws-amplify/api').default

import { Link } from "../routes"
import { configureAmplify } from "../config"

configureAmplify()

const Navigation = (props) => {
    const [menu, useMenu] = useState([])
    useEffect(() => {
        const fetchMenu = async () => {
            const { data } = await API.get("pages", "/list", {
                headers: {},
                response: false,
                queryStringParameters: { pageID: 'menu', params: {} }
            })
            useMenu(data.nav)
        }
        fetchMenu()
    }, [])
    return menu ? (
        <React.Fragment>
            <Menu.Menu position={props.position}>
                {menu.map((nav, key) => {
                    return nav.items.length > 0 ? (
                        <Dropdown key={key} text={nav.primary.label[0].text} item>
                            <Dropdown.Menu>
                            {nav.items.map( (submenu, key) => (
                                <Link 
                                    key={key} 
                                    route={submenu.sub_nav_link.type}
                                    params={{slug: submenu.sub_nav_link.uid}}
                                    prefetch
                                    >
                                    <Dropdown.Item text={submenu.sub_nav_link_label[0].text} />
                                </Link>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                    ) :(
                        <Menu.Item key={key} link>
                            <Link route={nav.primary.link.type.toLowerCase().replace("_", "-")} prefetch>
                                <a>{nav.primary.label[0].text}</a>
                            </Link>
                        </Menu.Item>
                    )})
                }
            </Menu.Menu>
        </React.Fragment>
    ):  <React.Fragment />
}
                
const Logo = (props) => (
    <Menu.Item position={props.position} link>
        <Link route='home' prefetch>
            <Image src="/static/logo.png" size="small" />
        </Link>
    </Menu.Item>
)
                
export {Navigation, Logo }
                
