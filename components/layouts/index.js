import React from "react"

import { Segment, Grid, Container } from "semantic-ui-react"

import ResponsiveContainer from "./containers/ResponsiveContainer"
import Footer from "../Footer"


const Layout = (props) => {   
    return (<ResponsiveContainer>
        <Segment color={"orange"} style={{padding: "4em 0em" }} vertical>
            <Container>
                <Grid centered columns={1} reversed>
                    <Grid.Column mobile="sixteen" computer="sixteen">
                        {props.children}
                    </Grid.Column>
                </Grid >
            </Container>
        </Segment>
        <Footer />
    </ResponsiveContainer>)
}


export default Layout