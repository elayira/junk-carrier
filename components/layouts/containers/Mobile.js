import React, { Component, createRef } from "react"

import {
    Icon,
    Menu,
    Ref,
    Segment,
    Sidebar,
} from "semantic-ui-react"

import {Navigation, Logo} from "../../navbar"
import Hero from "../../Hero"


export default class Mobile extends Component {
    state = {}
    segmentRef = createRef()

    handleShowClick = () => this.setState({ visible: true })

    handleSidebarHide = () => this.setState({ visible: false })

    render() {
        const { visible } = this.state
        const { children } = this.props

        return (
            <span>
                <Menu
                    inverted
                    pointing
                    secondary
                    size="large"
                    color="red"
                    style={{ margin: "0px", border: "none", borderRadius: "0" }}>
                    <Menu.Item onClick={this.handleShowClick}>
                        <Icon name="sidebar" size="big" inverted />
                    </Menu.Item>
                    <Logo position="right" />
                </Menu>

                <Sidebar.Pushable as={"span"}>
                    <Sidebar
                        as={Menu}
                        animation="overlay"
                        icon="labeled"
                        inverted
                        onHide={this.handleSidebarHide}
                        vertical
                        target={this.segmentRef}
                        visible={visible}
                        width="thin"
                    >
                        <Navigation />
                    </Sidebar>
                    <Ref innerRef={this.segmentRef}>
                        <Segment style={{ padding: "0", margin: "0", border: "none", borderRadius: "none" }}>
                            <Hero />
                        </Segment>
                    </Ref>

                    {children}
                </Sidebar.Pushable>
            </span>
        )
    }
}