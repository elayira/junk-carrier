import React from "react"

import {
    Container,
    Menu,
    Responsive,
    Segment,
    Visibility,
} from "semantic-ui-react"

import {viewportWidth} from "../../helpers/utility"
import {Navigation, Logo} from "../../navbar"
import Hero from "../../Hero"


class Desktop extends React.Component {
    state = {}

    hideFixedMenu = () => this.setState({ fixed: false })
    showFixedMenu = () => this.setState({ fixed: true })

    render() {
        const { children } = this.props
        const { fixed } = this.state

        return (
            <Responsive getWidth={viewportWidth} minWidth={Responsive.onlyTablet.minWidth}>
                <Visibility
                    once={false}
                    onBottomPassed={this.showFixedMenu}
                    onBottomPassedReverse={this.hideFixedMenu}
                >
                    <Segment
                        style={{
                            padding: "0em 0em",
                            backgroundColor: "#000",
                            margin: "0"
                        }}
                        textAlign="center"
                        vertical
                    >
                        <Menu
                            fixed={fixed ? "top" : null}
                            pointing={!fixed}
                            color="red"
                            inverted
                            borderless
                            style={{ margin: "0 0", borderRadius: "0" }}
                        >
                            <Container fluid>
                                <Logo />
                                <Navigation position="right" />
                            </Container>
                        </Menu>
                        <Hero />
                    </Segment>
                </Visibility>
                {children}
            </Responsive>
        )
    }
}

export default Desktop
