import React from "react"

import {Responsive} from "semantic-ui-react"

import Desktop from "./Desktop"
import Mobile from "./Mobile"
import {viewportWidth} from "../../helpers/utility"


const ResponsiveContainer = (props) => (
    <div>
        <Desktop>{props.children}</Desktop>
        <Responsive getWidth={viewportWidth} maxWidth={Responsive.onlyMobile.maxWidth} >
            <Mobile>{props.children}</Mobile>
        </Responsive>
    </div>
)


export default ResponsiveContainer
