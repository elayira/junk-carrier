import React from "react"

import {
    Segment,
    Container,
    Grid,
    Header,
    List,
} from "semantic-ui-react"

import { SOCIAL_MEDIA, DEFAULT_SEO } from "../config"

export default () => (
    <Segment inverted color={"black"} vertical style={{ paddingTop: "5em" }}>
        <Container>
            <Grid inverted>
                <Grid.Row>
                    <Grid.Column computer="six" mobile="six">
                        <Header color="red" as="h3" content={DEFAULT_SEO.title} />
                        <List link inverted>
                            <List.Item as="a">Privacy</List.Item>
                            <List.Item as="a">Cookie Policy</List.Item>
                            <List.Item as="a">Legal</List.Item>
                        </List>
                    </Grid.Column>
                    <Grid.Column computer="six" mobile="six">
                        <Header as="h3" color="red" content="Services" />
                        <List link inverted>
                            <List.Item as="a">Currency Conversion</List.Item>
                            <List.Item as="a">Realtime FX Rate</List.Item>
                            <List.Item as="a">BDC Rate</List.Item>
                            <List.Item as="a">ATM Rate</List.Item>
                        </List>
                    </Grid.Column>
                    <Grid.Column computer="four" mobile="sixteen" >
                        <Header as="h3" color="red" content="About" />
                        <p>
                            {DEFAULT_SEO.description}
                        </p>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16} textAlign={"center"}>
                        <List horizontal relaxed size={"big"}>
                            <List.Item>{DEFAULT_SEO.title} &copy; {new Date().getFullYear()}</List.Item>
                            {SOCIAL_MEDIA.map((mediaNetwork, key) => (
                                <List.Item key={key}>
                                    <a href={mediaNetwork.url}>
                                        <List.Icon name={mediaNetwork.name} circular inverted size="big" />
                                    </a>
                                </List.Item>
                            ))}
                        </List>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    </Segment>
)
